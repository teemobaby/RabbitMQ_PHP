<?php
/**
 * Created by PhpStorm.
 * User: lkk
 * Date: 2018/12/12
 * Time: 8:22 PM
 */
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('teemo', false, false, false, false);

echo " [*] Waiting for messages. To exit press CTRL+C\n";



$callback = function ($msg) {
    echo ' [x] Received ', $msg->body, "\n";
};

$channel->basic_consume('teemo', '', false, true, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}